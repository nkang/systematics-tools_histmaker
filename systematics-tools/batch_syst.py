#!/usr/bin/env python

import os

indir="/sdf/home/n/nkang/workdir/batch/systematics-tools_histmaker/hists_output_v14_mjjtest"

l_samples = [
    "EW_Wjets_Sh2211",
    "EW_Zjets_Sh2211",
    "Strong_Wjets_Sh2211",
    "Strong_Zjets_Sh2211",
]

# list of lists of [aoname,cut_name]
l_regions = [
#     ["BDTscore","SR_0L_test1"],
#     ["BDTscore","SR_0L_test2"],
#     ["BDTscore_LepInvis","CR_1L_test1"],
#     ["BDTscore_LepInvis","CR_1L_test2"],
#     ["BDTscore_LepInvis","CR_2L_test1"],
#     ["BDTscore_LepInvis","CR_2L_test2"],
#     ["BDTscore","SR_0L_mjj500"],
#     ["BDTscore_LepInvis","CR_1L_mjj500"],
#     ["BDTscore_LepInvis","CR_2L_mjj500"],
#     ["BDTscore","SR_0L_mjj600"],
#     ["BDTscore_LepInvis","CR_1L_mjj600"],
#     ["BDTscore_LepInvis","CR_2L_mjj600"],
#     ["BDTscore","SR_0L_mjj700"],
#     ["BDTscore_LepInvis","CR_1L_mjj700"],
#     ["BDTscore_LepInvis","CR_2L_mjj700"],
#     ["BDTscore","SR_0L_mjj800"],
#     ["BDTscore_LepInvis","CR_1L_mjj800"],
#     ["BDTscore_LepInvis","CR_2L_mjj800"],
    ["BDTscore","SR_0L_mjj900"],
    ["BDTscore_LepInvis","CR_1L_mjj900"],
    ["BDTscore_LepInvis","CR_2L_mjj900"],
    ["BDTscore","SR_0L_mjj1000"],
    ["BDTscore_LepInvis","CR_1L_mjj1000"],
    ["BDTscore_LepInvis","CR_2L_mjj1000"],
]

for s in l_samples:
    for l in l_regions:
        cmd = "sbatch batch_syst.sh {} {} {} {}".format(indir,l[0],s,l[1])
        os.system(cmd)