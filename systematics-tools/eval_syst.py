#!/usr/bin/env python

import systematicsTool as st #import the systematicl tool 
import readDatabase as rdb #import the systematic database
import os,sys,pprint
import ROOT
import json
import numpy as np

d_npz = {
    '[scale_ME]': "scale",
    '[PDF_ME]': "pdf",
    '[alphaS]': "alphaS",
}

# for renaming npz files as needed
d_cuts_to_regs = {
#     "SR_0L_test1": "SR_VBF",
#     "CR_1L_test1": "CR_1L",
#     "CR_2L_test1": "CR_2L",
#     "SR_0L_test2": "SR_VBF",
#     "CR_1L_test2": "CR_1L",
#     "CR_2L_test2": "CR_2L",
#     "SR_0L_mjj500": "SR_VBF",
#     "CR_1L_mjj500": "CR_1L",
#     "CR_2L_mjj500": "CR_2L",
#     "SR_0L_mjj600": "SR_VBF",
#     "CR_1L_mjj600": "CR_1L",
#     "CR_2L_mjj600": "CR_2L",
#     "SR_0L_mjj700": "SR_VBF",
#     "CR_1L_mjj700": "CR_1L",
#     "CR_2L_mjj700": "CR_2L",
#     "SR_0L_mjj800": "SR_VBF",
#     "CR_1L_mjj800": "CR_1L",
#     "CR_2L_mjj800": "CR_2L",
    "SR_0L_mjj900": "SR_VBF",
    "CR_1L_mjj900": "CR_1L",
    "CR_2L_mjj900": "CR_2L",
    "SR_0L_mjj1000": "SR_VBF",
    "CR_1L_mjj1000": "CR_1L",
    "CR_2L_mjj1000": "CR_2L",
}

indir=sys.argv[1]

aoname = sys.argv[2] # the var name
sample_name = sys.argv[3]
cut_name = sys.argv[4]

my_name = "{}_{}_{}".format(aoname,sample_name,cut_name)
outdir="results/"+my_name
inF=my_name+".root"

regexFilter=None #you filter for only certain histogram names
regexVeto=None #you can veto any histogram names you wish to skip

# weightList=rdb.getWeights('700201')[0] #retrieve the weights for this samples and their combination recipes
import WeightList
weightList = WeightList.weightDict[sample_name]
pprint.pprint(weightList)
schema="!INDIR/!INFILE.root:!AONAME__!NSFWEIGHTNAME" #define the schema, ie the naming convention of how you stored the analysis objects in your files(s) - see dedicated section on this topic in the documentations
os.system("mkdir -p %s" %outdir)

result= st.combineAllVariations(weightList, indir, outdir,regexFilter=regexFilter, regexVeto=regexVeto, 
returnOnlyVariationsInComination=True, schema=schema, inFile=inF) #automagically combine the per-weight histograms into per-variation analysis objects!

print(result)

for f,n in result.items():
    if n == "[Nominal]": continue
    tf = ROOT.TFile.Open(f,"READ")
    tg = tf.Get(aoname)
    up_ratios = []
    down_ratios = []
    for i in xrange(tg.GetN()):
        if tg.GetErrorYhigh(i) < 0 or tg.GetErrorYlow(i) < 0:
            raise Exception("Negative error value found when assumed to be positive")
        if not tg.GetY()[i] == 0:
            up_ratios.append( 1. + (tg.GetErrorYhigh(i)/tg.GetY()[i]) )
            down_ratios.append( 1. - (tg.GetErrorYlow(i)/tg.GetY()[i]) )
        else:
            up_ratios.append( 1 )
            down_ratios.append( 1 )
    d_ratios = {"up":up_ratios, "down":down_ratios}
    with open(f.replace(".root",".json"),"w") as outfile:
        json.dump(d_ratios,outfile)
    if n in d_npz:
        print("Writing out npz files for: "+d_npz[n])
        # write npz histogram files with yields for cabinetry
        h_up = tf.Get(aoname+"__1up")
        h_down = tf.Get(aoname+"__1down")
        if [float(h_up.GetBinLowEdge(i)) for i in range(1,int(h_up.GetNbinsX()+2))] != [float(h_down.GetBinLowEdge(i)) for i in range(1,int(h_down.GetNbinsX()+2))]:
            raise Exception("Binning between up and down do not match")
        binning_temp = [float(h_up.GetBinLowEdge(i)) for i in range(1,int(h_up.GetNbinsX()+2))]
        if len(binning_temp) > 2:
            binning_temp = binning_temp[:-1] # remove last bin edge for cabinetry overflow hack
        samp_name = sample_name
        syst_name = "{}_{}".format(samp_name,d_npz[n])
        reg_name = cut_name
        if cut_name in d_cuts_to_regs:
            reg_name = d_cuts_to_regs[cut_name]
        outfile_up = '{}/{}_{}_{}_Up_modified.npz'.format(outdir,reg_name,samp_name,syst_name)
        outfile_down = '{}/{}_{}_{}_Down_modified.npz'.format(outdir,reg_name,samp_name,syst_name)
        yields_up = [float(h_up.GetBinContent(i)) for i in range(1,int(h_up.GetNbinsX()+1))]
        yields_down = [float(h_down.GetBinContent(i)) for i in range(1,int(h_down.GetNbinsX()+1))]
        error_up = [float(h_up.GetBinError(i)) for i in range(1,int(h_up.GetNbinsX()+1))] # this includes only zeroes but is ok
        error_down = [float(h_down.GetBinError(i)) for i in range(1,int(h_down.GetNbinsX()+1))]
        np.savez(outfile_up,yields=yields_up,stdev=error_up,bins=binning_temp)
        np.savez(outfile_down,yields=yields_down,stdev=error_down,bins=binning_temp)
    
#optionally make some nice plots
plots=st.makeSystematicsPlotsWithROOT(result, outdir, nominalName='_Nominal', label="" ,ratioZoom=None, regexFilter=regexFilter, regexVeto=regexVeto)
