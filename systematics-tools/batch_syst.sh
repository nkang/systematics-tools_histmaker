#!/bin/bash

#SBATCH --partition=shared
#
#SBATCH --job-name=systematics-tools
#SBATCH --output=output-%j.txt
#SBATCH --error=output-%j.txt
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=3g
#
#SBATCH --time=0:10:00
#

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
setupATLAS() {
        source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
}

cd /sdf/home/n/nkang/workdir/systematics-tools
source setupSystematicsTool.sh
./eval_syst.py $1 $2 $3 $4