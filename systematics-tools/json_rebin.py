#!/usr/bin/env python3

import glob
import json

l_files = glob.glob("/sdf/home/n/nkang/workdir/systematics-tools/results_v14_CRsinglebin_srBDT0p6/*SR*/*.json")
my_slice = slice(4,None)

for f in l_files:
    with open(f, "r") as jsonFile:
        data = json.load(jsonFile)

    data["down"] = data["down"][my_slice]
    data["up"] = data["up"][my_slice]

    with open(f, "w") as jsonFile:
        json.dump(data, jsonFile)
        
    print(f"Modified file {f}",flush=True)