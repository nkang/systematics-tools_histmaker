#!/usr/bin/env python3

import argparse
import json
import numpy
import os
import ROOT
import time

ROOT.TTree.SetMaxTreeSize( 1000000000000 )
ROOT.TH1.SetDefaultSumw2(True)
ROOT.EnableImplicitMT(2)

parser = argparse.ArgumentParser(description="Input histogram maker for systematics-tools")
parser.add_argument('sample_name', help="sample name as a key to read from samples.json with file paths")
parser.add_argument('tree_name', help="tree name")
parser.add_argument('cuts_name', help="cuts name as key to read from cuts.json with cuts to apply")
args = parser.parse_args()

# key = Variable for histogram binning; value = Binning, assumes last is overflow except for single bin
from d_binning import d_vars

# Standard region naming convention
if not any(reg_sub in args.cuts_name for reg_sub in ("SR","CR","VR")):
    raise Exception("Invalid cuts name")

# Prevent SR data unblinding
if args.sample_name == "data" and "SR" in args.cuts_name:
    raise Exception("Unblinding data in SR not allowed")

with open("./samples.json") as f:
    d_samples = json.load(f)["samples"]
f_cuts = "./cuts.json"
with open(f_cuts) as f:
    j_cuts = json.load(f)
    d_cuts = j_cuts["cuts"]
    weights = j_cuts["weights"]

sample_path = d_samples[args.sample_name]
cut_string = d_cuts[args.cuts_name]

timer_start = time.time()
print("Started timer...",flush=True)

print(f"Sample name: {args.sample_name}")

print(f"Reading from file {sample_path}")

tf = ROOT.TFile(sample_path,"READ")

print(f"Getting tree {args.tree_name}")

tree = tf.Get(args.tree_name)

# Assume the weight names are always the same order
tree.GetEntry(0)
l_wnames = [str(x) for x in tree.LHE3WeightNames]

df = ROOT.RDataFrame(tree)

print(f"Applying cuts {args.cuts_name}: {cut_string}")
print(f"Applying weights: {weights}")

if args.sample_name == "data":
    weights = "1."

df_skim = df.Filter(cut_string).Define("weight",weights)

print(f"Using these variables with specified binning: {d_vars}")
# Should book filling of all the requested histograms so they can be filled in parallel in same loop
node = df_skim.Vary("weight","LHE3Weights * weight",l_wnames)
d_hists = {}
for v in d_vars:
    binning = d_vars[v]
    if len(binning) > 2:
        n_bins = len(binning)-2
        binning_temp = binning[:-1]
    else:
        n_bins = 1
        binning_temp = binning
    d_hists[v] = node.Histo1D( 
        ( v,v,n_bins,numpy.array(binning_temp,dtype='float64') ),
        v,
        f"weight" )
varyName = "weight"
l_keys = [f"{varyName}:{w}" for w in l_wnames]
d_hx = {}
for v in d_hists:
    d_hx[v] = ROOT.RDF.Experimental.VariationsFor(d_hists[v]) # VariationsFor does not trigger the event loop. The event loop is only triggered upon first access to a valid key
for v in d_hists:
    binning = d_vars[v]
    hx = d_hx[v]
    outdir = "hists_output"
    outname = f"{outdir}/{v}_{args.sample_name}_{args.cuts_name}.root"
    os.system(f"mkdir -p {outdir}")
    outfile = ROOT.TFile.Open(outname,"RECREATE")
    for k in l_keys:
        h = ROOT.TH1D(v,v,len(binning)-1,numpy.array(binning,dtype='float64'))
        for i in range( hx[k].GetNbinsX()+1 ):
            h.SetBinContent( i+1, hx[k].GetBinContent(i+1) )
            h.SetBinError( i+1, hx[k].GetBinError(i+1) )
        outfile.WriteObject(h,f"{v}__" + k.strip(f"{varyName}:"))
        h.Delete()
    outfile.Close()


print(f"\nTime to finish: {time.time() - timer_start} s\n")
print(f"GetNRuns: {df_skim.GetNRuns()}\n")