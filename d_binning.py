import numpy

def lbin(start,stop,size):
    return list( numpy.linspace(start,stop,size) )
# key = Variable for histogram binning; value = Binning, assumes last is overflow
d_vars = {
#     "vbfjjM": [500, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000],
#     "met_Et": [250,275,300,325,350,375,400,425,450,475,500,525,550,600,700,800],
#     "met_Et_LepInvis": [250,275,300,325,350,375,400,425,450,475,500,525,550,600,700,800],
    "BDTscore": [0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,1],
    "BDTscore_LepInvis": [0.4,1],
#     "BDTscore_LepInvis": [0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,1],
}