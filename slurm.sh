#!/bin/bash

#SBATCH --partition=shared
#
#SBATCH --job-name=systematics-tools_histmaker
#SBATCH --output=output-%j.txt
#SBATCH --error=output-%j.txt
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=2
#SBATCH --mem-per-cpu=3g
#
#SBATCH --time=0:30:00
#

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
setupATLAS() {
        source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
}

cd /sdf/home/n/nkang/workdir/batch/systematics-tools_histmaker
source setup.sh
./systematics-tools_histmaker.py $1 $2 $3