#!/usr/bin/env python3

import os
import pprint

l_samples = [
    'EW_Wjets_Sh2211',
    'EW_Zjets_Sh2211',
    'Strong_Wjets_Sh2211',
    'Strong_Zjets_Sh2211',
]

tree_suffix = "_NoSys"

l_cuts = [
#     "SR_0L_test1",
#     "CR_1L_test1",
#     "CR_2L_test1",
#     "SR_0L_test2",
#     "CR_1L_test2",
#     "CR_2L_test2",
#     "SR_0L_mjj500",
#     "CR_1L_mjj500",
#     "CR_2L_mjj500",
#     "SR_0L_mjj600",
#     "CR_1L_mjj600",
#     "CR_2L_mjj600",
#     "SR_0L_mjj700",
#     "CR_1L_mjj700",
#     "CR_2L_mjj700",
#     "SR_0L_mjj800",
#     "CR_1L_mjj800",
#     "CR_2L_mjj800",
    "SR_0L_mjj900",
    "CR_1L_mjj900",
    "CR_2L_mjj900",
    "SR_0L_mjj1000",
    "CR_1L_mjj1000",
    "CR_2L_mjj1000",
]

for s in l_samples:
    for c in l_cuts:
        cmd = f"sbatch slurm.sh {s} {s}{tree_suffix} {c}"
        os.system(cmd)
    